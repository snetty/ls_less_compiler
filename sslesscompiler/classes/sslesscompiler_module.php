<?php

	class SSLessCompiler_module extends Core_ModuleBase
	{
		/**
		 * Creates the module information object
		 * @return Core_ModuleInfo
		 */
		protected function createModuleInfo()
		{
			return new Core_ModuleInfo(
				"Less Compiler",
				"Automatically compiles less files",
				"3DPixel.net" );
		}
		
		public function subscribeEvents()
		{
			Backend::$events->addEvent('cms:onBeforeResourceCombine', $this, 'before_resource_combine');
		}
		
		public function before_resource_combine($array){
			require_once('lessc.php');
			if($array['type'] == 'css'){
				foreach($array['files'] as $file){
					if(strpos($file, '.css') !== false){
						if(strpos($file, '@') !== false) $file = theme_resource_url(str_replace('@', '', $file));
						$css = PATH_APP . $file;
						$less = str_replace('.css', '.less', $css);

						if(file_exists($less))
							self::auto_compile_less($less, $css);
						
						/*
						if(file_exists($less)){
							$cache = lessc::cexecute($less, $css);
							file_put_contents($css_file, $cache['compiled']);
						}
						*/
					}
				}
			}
		}
		
		static function auto_compile_less($less_fname, $css_fname) {
			// load the cache
			$cache_fname = $less_fname.".cache";
			if (file_exists($cache_fname)) {
				$cache = unserialize(file_get_contents($cache_fname));
			} else {
				$cache = $less_fname;
			}
			
			$new_cache = lessc::cexecute($cache);
			if (!is_array($cache) || $new_cache['updated'] > $cache['updated']) {
				file_put_contents($cache_fname, serialize($new_cache));
				file_put_contents($css_fname, $new_cache['compiled']);
			}
		}
	}
?>